
### proxy for wget to get raw-files
export HTTPSproxy?=""


### setting default paths of external libraries
ITKLIB?=/opt/itk-4.13.2/lib/cmake/ITK-4.13
VTKLIB?=/opt/vtk-8.1.2/lib/cmake/vtk-8.1
### setting default paths of external programs

## path to submodules
export SUBDIR = $(realpath submodules)

### setting default paths of internal programs for PATH
ITK?=$(SUBDIR)/ITK-CLIs/
VTK?=$(SUBDIR)/VTK-CLIs/
ITKVTK?=$(SUBDIR)/ITK-VTK_CLIs/


SHELL:= /bin/bash


export VGLRUN?=vglrun

export PATH:= $(ITK)/build:$(PATH)
export PATH:= $(VTK)/build:$(PATH)
export PATH:= $(ITKVTK)/build:$(PATH)


### check existance of external programs
## http://stackoverflow.com/questions/5618615/check-if-a-program-exists-from-a-makefile#25668869
ITKEXE = subimage_extract subimage_extract_series bin file-series_reader file_converter resample anisoDiff-grad_f32 gradient_mag_f64 watershed_morph_f32_UI32 std-mean_ROI_SBS thresh-glob thresh-glob fill_holes skeletonize_3D fast-marching_f32 distance_map_parabolic_f32 distance_map_signed_maurer_f32 segmentLung paste_image stat open_bin_para max watershed_morph_marker_UI32 toUInt16 shift-scale_window_UI8 mask mask-negated thresh-erase keepNobj label_connected_components hist min-path_seg_f32 sigmoid slice subtract open_label-shape erode-dilate_dm_f32 analyse_labels mean erode_gray file-series_writer
VTKEXE = mc_discrete probe-surf enclosed_points threshold analyse_S+V dupli-cell_removal decimate-QC vtk2vtp probe-surf2ply marching-cubes
ITKVTKEXE = straighten
## external programs


K:= $(foreach exec,$(EXECUTABLES),\
	$(if $(shell PATH=$(PATH) which $(exec)),some string,$(error "No $(exec) in PATH")))




## SUBDIRS should not contain targets to be executed before processing/
SUBDIRS:= process/


.PHONY: all clean $(SUBDIRS)


all : $(SUBDIRS)

clean :
	$(MAKE) -C $(SUBDIRS) clean


## build internal tools
## only build those listed above e.g. ITKEXE
## run with unlimited -j because all involved programms are single threaded, needs spedific rules (intTools.mk) because multiple goals are processed serially ("in turn") even with -j: https://savannah.gnu.org/support/?107274
.PHONY: intTools # make sure intTools is always executed (even if intTools.done already exists)
intTools :
	git submodule update --init --recursive # http://stackoverflow.com/questions/3796927/how-to-git-clone-including-submodules#4438292
	$(MAKE) \
		ITKLIB=$(ITKLIB) ITKEXE='$(ITKEXE)' \
		VTKLIB=$(VTKLIB) VTKEXE='$(VTKEXE)' \
		VTKLIB=$(VTKLIB) ITKLIB=$(ITKLIB) ITKVTKEXE='$(ITKVTKEXE)' \
		-j32 -f intTools.mk # unlimited -j overridden by -j6 from build.sh?
	INTTOOLS="$(ITKEXE) $(VTKEXE) $(ITKVTKEXE)"; \
		PATH=$(PATH); \
		for i in $$INTTOOLS; do if test -z `which $$i`; then echo "Error, No $$i in PATH!" 1>&2; exit 125; fi; done


$(SUBDIRS) : | intTools # order only dep to prevent reexec

$(SUBDIRS) :
	/usr/bin/time -v -o $(dir $@)timing \
	   $(MAKE) -C $(dir $@)

